<?php


class CriticalPathCSSManager extends ModelAdmin {

    private static $menu_title = 'Critical Path';
    private static $url_segment = 'critical-path-css';
    private static $menu_icon = 'criticalcss/images/CriticalPathCSSManager.png';
    private static $managed_models = array(
        'CriticalPathCSSAction' => array(
            'title' => 'Actions'
        ),
        'CriticalPathCSSRequest' => array(
            'title' => 'Pending'
        )
    );
    private static $css_path;

    private $_css_path;

    private function _getCSSPath() {
        if (is_null($this->_css_path)) {
            $path = $this->extend('getFullCSSPath');
            if (!empty($path)) {
                $this->_css_path = $path[0];
            }
            else {
                $this->_css_path = Config::inst()->get(get_called_class(), 'css_path');
            }
        }
        return $this->_css_path;
    }

    private $_css_updated;

    private function _getCSSUpdated() {
        if (is_null($this->_css_updated)) {
            $path = sprintf('%s/%s', $_SERVER['DOCUMENT_ROOT'], $this->_getCSSPath());
            if (file_exists($path)) {
                $this->_css_updated = date('Y-m-d H:i:s', filemtime($path));
            }
            else {
                $this->_css_updated = false;
            }
        }
        return ($this->_css_updated === false) ? null : $this->_css_updated;
    }

    private $_actions;

    private function _getActions() {
        if (is_null($this->_actions)) {
            $this->_actions = ArrayList::create();
            $action_maps = array();
            $maps = $this->extend('getCriticalPathCSSActions');
            foreach ($maps as $map) {
                $action_maps = array_merge($action_maps, $map);
            }
            foreach ($action_maps as $map) {
                $map['ID'] = $this->_actions->count() + 1;
                $map['CSSUpdated'] = $this->_getCSSUpdated();
                $this->_actions->push(CriticalPathCSSAction::create($map));
            }
        }
        return $this->_actions;
    }

    public function getEditForm($id=null, $fields=null) {
        $form = parent::getEditForm($id, $fields);
        if ($this->modelClass == 'CriticalPathCSSAction') {
            if ($field = $form->Fields()->dataFieldByName('CriticalPathCSSAction')) {
                if ($field instanceof GridField) {
                    $columns = new GridFieldDataColumns();
                    $columns->setDisplayFields(
                        Config::inst()->get('CriticalPathCSSAction', 'summary_fields')
                    );
                    $field->setTitle('Actions')->setConfig(GridFieldConfig::create()->addComponents(
            			new GridFieldToolbarHeader(),
                        new GridFieldCriticalCSSPlainHeader(),
            			$columns,
                        new GridFieldCriticalCSSCacheStatus(),
                        new GridFieldCriticalCSSRowButton('UpdateCSSAction', 'Update', function($action) {
                            if ($action->getRequests()->count() < 1) {
                                if ($date = $action->getLastCache()) {
                                    $date = strtotime($date);
                                    $css_updated = $action->getCSSUpdated();
                                    if (!is_null($css_updated)) {
                                        return strtotime($css_updated) > $date;
                                    }
                                }
                                else {
                                    return true;
                                }
                                return false;
                            }
                            else {
                                return "Update Requested: <span>{$action->getRequests()->first()->Status}</span>";
                            }
                        }, function($action, $data) {
                            CriticalPathCSSAPI::generate($action->formRequest());
                        }),
                        new GridFieldCriticalCSSRowButton('ClearCSSCache', 'Clear Cache', function($action) {
                            return !is_null($action->getLastCache());
                        }, function($action, $data) {
                            $action->clearCSSCache();
                        }),
                        new GridFieldCriticalCSSListButton('ClearAllCSSCache', 'Clear Entire Cache', function($list, $data) {
                            foreach (CriticalPathCSSCache::get() as $cache) {
                                $cache->delete();
                            }
                        }),
                        new GridFieldCriticalCSSListButton('UpdateAllCSSActions', 'Update All', function($list, $data) {
                            foreach ($list as $action) {
                                $update = false;
                                if ($action->getRequests()->count() < 1) {
                                    $css_updated = $action->getCSSUpdated();
                                    if (!is_null($css_updated)) {
                                        if ($date = $action->getLastCache()) {
                                            if ($css_updated > strtotime($date)) {
                                                $update = true;
                                            }
                                        }
                                        else {
                                            $update = true;
                                        }
                                    }
                                }
                                if ($update) {
                                    CriticalPathCSSAPI::generate($action->formRequest());
                                }
                            }
                        }),
                        new GridFieldCriticalCSSStatus($this->_getCSSUpdated())
                    ));
                }
            }
        }
        else if ($this->modelClass == 'CriticalPathCSSRequest') {
             if ($field = $form->Fields()->dataFieldByName('CriticalPathCSSRequest')) {
                if ($field instanceof GridField) {
                    $field->setTitle('Pending')->setConfig(GridFieldConfig::create()->addComponents(
            			new GridFieldToolbarHeader(),
                        new GridFieldCriticalCSSPlainHeader(),
            			new GridFieldDataColumns(),
                        new GridFieldCriticalCSSRowButton('UpdateCSSRequest', 'Update', function($request) {
                            return in_array($request->Status, array('Unknown', 'Queued', 'Ongoing'));
                        }, function($request, $data) {
                            CriticalPathCSSAPI::result($request);
                        }),
                        new GridFieldDeleteAction(),
                        new GridFieldCriticalCSSListButton('ClearCSSRequests', 'Clear All', function($list, $data) {
                            foreach ($list as $request) {
                                if (!in_array($request->Status, array('Ongoing', 'Queued'))) {
                                    $request->delete();
                                }
                            }
                        }),
                        new GridFieldCriticalCSSListButton('UpdateCSSRequests', 'Update All', function($list, $data) {
                            foreach ($list as $request) {
                                if (in_array($request->Status, array('Unknown', 'Queued', 'Ongoing'))) {
                                    CriticalPathCSSAPI::result($request);
                                }
                            }
                        })
                    ));
                }
            }
        }
        return $form;
    }

    public function getList() {
        if ($this->modelClass == 'CriticalPathCSSAction') {
            return $this->_getActions();
        }
        else {
            return parent::getList();
        }
    }
}
