<?php


class CriticalPathCSS_SiteTree extends DataExtension {

    public function contentcontrollerInit($controller) {
        if (get_class(Requirements::backend()) == 'Requirements_Backend') {
            Requirements::set_backend(new CriticalPathCSS_Requirements_Backend());
        }
    }
}

class CriticalPathCSS_ContentController extends Extension {

    public function beforeCallActionHandler($request, $action) {
        CriticalPathCSS_Requirements_Backend::configure_cache($this->owner->dataRecord->ClassName, $action);
    }
}
