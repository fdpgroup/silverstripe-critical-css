<?php


class CriticalPathCSSAction extends ViewableData {

    private static $casting = array(
        'Type' => 'Varchar',
        'Action' => 'Varchar',
        'URL' => 'Varchar',
        'CSSUpdated' => 'SS_Datetime',
        'LastCache' => 'SS_Datetime'
    );
    private static $summary_fields = array(
        'Type' => 'Type',
        'Action' => 'Action',
        'URL' => 'URL'
    );

    public function __construct() {
        $args = func_get_args();
        if (count($args) > 0) {
            $map = $args[0];
            $this->_id = $map['ID'];
            $this->_css_updated = $map['CSSUpdated'];
            $this->_type = $map['Type'];
            $this->_action = array_key_exists('Action', $map) ? strtolower($map['Action']) : 'index';
            $this->_url = array_key_exists('URL', $map) ? $map['URL'] : call_user_func_array(array($this->_type, 'get'), array())->first()->Link();
        }
    }

    private $_id;

    public function getID() {
        return $this->_id;
    }

    private $_type;

    public function getType() {
        return $this->_type;
    }

    private $_action;

    public function getAction() {
        return $this->_action;
    }

    private $_url;

    public function getURL() {
        return preg_match('/^http(s)?/', $this->_url) ? $this->_url : Director::absoluteURL($this->_url);
    }

    private $_css_updated;

    public function getCSSUpdated() {
        return $this->_css_updated;
    }

    private $_last_cache;

    public function getLastCache() {
        if (is_null($this->_last_cache)) {
            $cache = CriticalPathCSSCache::get()->filter(array(
                'Type' => $this->getType(),
                'Action' => $this->getAction()
            ))->sort('Created DESC')->first();
            if (is_null($cache)) {
                $this->_last_cache = false;
            }
            else {
                $this->_last_cache = $cache->Created;
            }
        }
        return ($this->_last_cache === false) ? null : $this->_last_cache;
    }

    public function clearCSSCache() {
        $cache = CriticalPathCSSCache::get()->filter(array(
            'Type' => $this->getType(),
            'Action' => $this->getAction()
        ));
        foreach ($cache as $cached) {
            $cached->delete();
        }
    }

    private $_requests;

    public function getRequests() {
        if (is_null($this->_requests)) {
            $this->_requests = CriticalPathCSSRequest::get()->filter(array(
                'Type' => $this->getType(),
                'Action' => $this->getAction()
            ));
        }
        return $this->_requests;
    }

    public function formRequest() {
        return CriticalPathCSSRequest::create(array(
            'Type' => $this->getType(),
            'Action' => $this->getAction(),
            'URL' => $this->getURL()
        ));
    }

    public function i18n_singular_name() {
        return 'Action';
    }

    public function plural_name() {
        return 'Actions';
    }

    public function canView() {
        return true;
    }

    public function canCreate() {
        return false;
    }

    public function canEdit() {
        return false;
    }

    public function canDelete() {
        return false;
    }

    public function getDefaultSearchContext() {
        return new SearchContext('CriticalPathCSSAction');
    }

    public function summaryFields() {
        return array();
    }
}
