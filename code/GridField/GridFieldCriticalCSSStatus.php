<?php


class GridFieldCriticalCSSStatus implements GridField_HTMLProvider {

    private $_css_updated;

    public function __construct($css_updated) {
        $this->_css_updated = $css_updated;
    }

    public function getHTMLFragments($grid) {
        if (!is_null($this->_css_updated)) {
            return array(
                'after' => sprintf(
                    '<p class="grid-field-css-status">Last CSS Filesystem Update: <span>%s</span></p>',
                    date('l jS F Y H:i:s', strtotime($this->_css_updated))
                )
            );
        }
        else {
            return array(
                'after' => '<p class="grid-field-css-status error"><span>CSS Not Found</span></p>'
            );
        }
    }
}
