<?php


class GridFieldCriticalCSSListButton implements GridField_HTMLProvider, GridField_ActionProvider {

    private $_name, $_title, $_click, $_target;

    public function __construct($name, $title, Closure $click, $target = 'toolbar-header-right') {
        $this->_name = $name;
        $this->_title = $title;
		$this->_click = $click;
        $this->_target = $target;
    }

    public function getHTMLFragments($grid) {
        $button = GridField_FormAction::create(
            $grid,
            $this->_name,
            $this->_title,
            $this->_name,
            null
        )->addExtraClass('ss-ui-action-constructive');
        return array(
            $this->_target => $button->Field()
        );
    }

    public function getActions($grid) {
        return array($this->_name);
    }

    public function handleAction(GridField $grid, $action, $args, $data) {
        if ($action == strtolower($this->_name)) {
            $callback = $this->_click;
            $callback($grid->getList(), $data);
            $grid->form->getController()->redirectBack();
        }
    }
}
