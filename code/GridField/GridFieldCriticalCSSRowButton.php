<?php


class GridFieldCriticalCSSRowButton implements GridField_ColumnProvider, GridField_ActionProvider {

    private $_name, $_title, $_display, $_click;

    public function __construct($name, $title, Closure $display, Closure $click) {
        $this->_name = $name;
        $this->_title = $title;
        $this->_display = $display;
		$this->_click = $click;
    }

    public function augmentColumns($grid, &$columns) {
        $columns[] = $this->_name;
    }

    public function getColumnAttributes($grid, $record, $name) {
        return array('class' => 'css-action');
    }

    public function getColumnMetadata($grid, $name) {
        if ($name == $this->_name) {
            return array('title' => '');
        }
    }

    public function getColumnsHandled($grid) {
        return array($this->_name);
    }

    public function getColumnContent($grid, $record, $column) {
        $callback = $this->_display;
        $display = $callback($record);
        if (is_string($display)) {
            return $display;
        }
        else {
            if ($display) {
                $id = $record->hasMethod('getID') ? $record->getID() : $record->ID;
                $field = GridField_FormAction::create(
                    $grid,
                    "{$this->_name}{$id}",
                    $this->_title,
                    $this->_name,
                    array('ID' => $id)
                )->addExtraClass('ss-ui-action-constructive');
                return $field->Field();
            }
            else {
                return '';
            }
        }
    }

    public function getActions($grid) {
        return array($this->_name);
    }

    public function handleAction(GridField $grid, $action, $args, $data) {
        if ($action == strtolower($this->_name)) {
            $record = $grid->getList()->filter('ID', $args['ID'])->first();
            if (!is_null($record)) {
                $callback = $this->_click;
                $callback($record, $data);
            }
        }
    }
}
