<?php


class GridFieldCriticalCSSPlainHeader implements GridField_HTMLProvider {

	public function getHTMLFragments($grid) {
		$data = new ArrayData(array());
		$data->Fields = new ArrayList();
		$columns = $grid->getColumns();
		$list = $grid->getList();

		foreach ($columns as $column) {
			$meta = $grid->getColumnMetadata($column);
			$field = LiteralField::create(str_replace('.', '-', $column), '<span class="non-sortable">' . $meta['title'] . '</span>');
			$data->Fields->push($field);
		}
		return array(
			'header' => $data->renderWith('GridFieldCriticalCSSPlainHeader_Row'),
		);
	}
}
