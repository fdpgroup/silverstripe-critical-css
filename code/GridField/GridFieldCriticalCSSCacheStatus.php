<?php


class GridFieldCriticalCSSCacheStatus implements GridField_ColumnProvider {

    public function augmentColumns($grid, &$columns) {
        $columns[] = 'CSSStatus';
    }

    public function getColumnAttributes($grid, $record, $name) {
        return array('class' => 'css-status');
    }

    public function getColumnMetadata($grid, $name) {
        if ($name == 'CSSStatus') {
            return array('title' => 'Last Cached');
        }
    }

    public function getColumnsHandled($grid) {
        return array('CSSStatus');
    }

    public function getActions($grid) {
        return array();
    }

    public function getColumnContent($grid, $record, $name) {
        if ($date = $record->getLastCache()) {
            $date = strtotime($date);
            $css_date = $record->getCSSUpdated();
            if (!is_null($css_date)) {
                $out_of_date = strtotime($css_date) > $date;
                return sprintf(
                    '<span class="%s"><span>%s:</span> %s</span>',
                    $out_of_date ? 'red' : 'green',
                    $out_of_date ? 'Out of Date' : 'Up-to-date',
                    date('l jS F Y H:i:s', $date)
                );
            }
            else {
                return sprintf('<span>%s</span>', date('l jS F Y H:i:s', $date));
            }
        }
        else {
            return '<span>Not Yet Cached</span>';
        }
    }

    public function handleAction(GridField $grid, $action, $args, $data) {

    }
}
