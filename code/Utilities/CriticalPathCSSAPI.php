<?php


class CriticalPathCSSAPI {

    private static $base_url = 'https://criticalcss.com/api/';
    private static $api_key = '';
    private static $debug = false;
    private static $log_path = 'tmp/critical-css-api.log';

    public static function ping() {
        return self::_call('GET', 'ping');
    }

    public static function generate($request) {
        $response = self::_call('POST', 'generate', true, array(), array(
			'height' => 900,
			'width' => 1300,
			'url' => $request->URL
		));
        if (is_object($response)) {
            if (property_exists($response, 'error')) {
                $request->Status = 'Failed';
                $request->Message = $response->error;
            }
            else {
                $request->APIID = $response->job->id;
                $request->updateStatus($response->job->status);
            }
        }
        else {
            $request->Status = 'Failed';
            $request->Message = $response;
        }
        $request->write();
        return $request;
    }

    public static function result($request) {
        $result = self::_call('GET', 'results', true, array(
            'resultId' => $request->APIID
        ));
        if (is_object($result)) {
            if (property_exists($result, 'error')) {
                $request->Status = 'Failed';
                $request->Message = $result->error;
            }
            else {
                $request->updateStatus($result->status);
                if ($request->Status == 'Done' && property_exists($result, 'css')) {
                    $cache = $request->formCache($result->css);
                    $cache->write();
                }
                else if ($request->Status == 'Queued' && property_exists($result, 'queueIndex')) {
                    $request->Message = "Current in position {$result->queueIndex} in queue";
                }
            }
        }
        else {
            $request->Status = 'Failed';
            $request->Message = $response;
        }
        $request->write();
        return $request;
    }

    private static function _call($method, $action, $auth = true, $querystring = array(), $args = array()) {
        $key = Config::inst()->get(get_called_class(), 'api_key');
        $debug = Config::inst()->get(get_called_class(), 'debug');
        if (!empty($key)) {
            $url = Config::inst()->get(get_called_class(), 'base_url');
            $headers = array(
                'Content-Type: application/json',
                'Accept-Language: en-US,en;q=0.8,sv;q=0.6',
                'Accept: application/json',
                'Connection: keep-alive'
            );
            if ($auth) {
    			$headers[] = "Authorization: JWT {$key}";
    			$url .= 'premium/';
    		}
            $url .= $action;
            if (count($querystring) > 0) {
                $url .= '?' . http_build_query($querystring);
            }
            $connection = curl_init($url);
            curl_setopt($connection, CURLOPT_HTTPHEADER, $headers);
    		curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);
            switch ($method) {
                case 'POST':
                    curl_setopt($connection, CURLOPT_POST, true);
                    break;
            }
            if (count($args) > 0) {
                curl_setopt($connection, CURLOPT_POSTFIELDS, json_encode($args));
            }

            if ($debug) {
                self::_log('API Called', array(
                    'URL' => $url,
                    'Method' => $method,
                    'Headers' => $headers,
                    'Authenticated' => $auth ? 'Yes' : 'No',
                    'Args' => $args
                ));
            }

            $data = curl_exec($connection);
    		if ($data === false) {
                $error = curl_error($connection);

                if ($debug) {
                    self::_log('API Failed', array(
                        'Error' => $error
                    ));
                }

                return $error;
            }
            else {
        		curl_close($connection);
                $data = json_decode($data);
                if ($debug) {
                    self::_log('API Response', $data);
                }
        		return $data;
            }
        }
		return null;
	}

    private static function _log($message, $data = null) {
        $path = Controller::join_links(BASE_PATH, Config::inst()->get(get_called_class(), 'log_path'));
        file_put_contents($path, sprintf("%s: %s\n%s\n\n%s", date('d/m/Y H:i:s'), $message, print_r($data, true), file_get_contents($path)));
    }
}
