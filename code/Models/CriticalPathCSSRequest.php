<?php


class CriticalPathCSSRequest extends DataObject {

    private static $db = array(
        'Type' => 'Varchar(200)',
        'Action' => 'Varchar(200)',
        'URL' => 'Varchar(255)',
        'APIID' => 'Varchar(200)',
        'Status' => "Enum('Unknown,Queued,Ongoing,Done,Failed')",
        'Message' => 'Varchar(200)',
        'Index' => 'Int'
    );
    private static $default_sort = 'Index ASC';

    private static $summary_fields = array(
        'Type' => 'Type',
        'Action' => 'Action',
        'URL' => 'URL',
        'APIID' => 'API ID',
        'Status' => 'Status',
        'Message' => 'Message'
    );

    public function onBeforeWrite() {
        parent::onBeforeWrite();
        if (!$this->Index) {
            $this->Index = CriticalPathCSSRequest::get()->max('Index') + 1;
        }
    }

    public function canView($member = null) {
        return true;
    }

    public function canCreate($member = null) {
        return false;
    }

    public function canEdit($member = null) {
        return false;
    }

    public function updateStatus($status) {
        $this->Message = '';
        switch ($status) {
            case 'JOB_ONGOING':
                $this->Status = 'Ongoing';
                break;
            case 'JOB_QUEUED':
                $this->Status = 'Queued';
                break;
            case 'JOB_DONE':
                $this->Status = 'Done';
                break;
            case 'JOB_UNKNOWN':
                $this->Status = 'Unknown';
                break;
            case 'JOB_FAILED':
                $this->Status = 'Failed';
                break;
        }
    }

    public function formCache($css) {
        return CriticalPathCSSCache::create(array(
            'Type' => $this->Type,
            'Action' => $this->Action,
            'URL' => $this->URL,
            'CSS' => $css
        ));
    }
}
