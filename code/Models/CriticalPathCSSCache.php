<?php


class CriticalPathCSSCache extends DataObject {

    private static $db = array(
        'Type' => 'Varchar(200)',
        'Action' => 'Varchar(200)',
        'URL' => 'Varchar(255)',
        'CSS' => 'Text'
    );
}
